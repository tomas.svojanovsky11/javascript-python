const people = [
    {
        id: 1,
        firstName: "Tomas",
        lastName: "Svojanovsky",
        birthday: "1991-09-08",
        gender: "male",
    },
    {
        id: 2,
        firstName: "Alfons",
        lastName: "Slepice",
        birthday: "1993-01-02",
        gender: "male",
    },
        {
        id: 3,
        firstName: "Petra",
        lastName: "Kolovratkova",
        birthday: "1987-05-07",
        gender: "male",
    },
];
